# README #

* A bird flu outbreak simulator that, with input for number of people, number of birds, and percent of infected birds, graphically displays a simulation of an outbreak scenario.

### How do I get set up? ###

* Run SimulationMain.java under epidemic.progpracticum.view.
* Enter values for number of people, number of birds, and percent of birds infected and press "generate world" 
* Start the simulation by pressing the start button at the bottom.
* The simulation will run until all birds and people are infected, at which point the user will be prompted to save the results.


### Who do I talk to? ###

* Christopher Helmer 
* christopher.helmer@yahoo.com