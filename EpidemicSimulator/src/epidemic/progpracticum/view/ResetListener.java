/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

/**
 * Action listener that resets the simulation timer, view, and stats.
 * @author Christopher Helmer
 * @version 1
 *
 */
public class ResetListener implements ActionListener {
    
    /**
     * The swing timer used in the simulation.
     */
    private Timer myTimer;
    /**
     * The SimWorld used in the simulation.
     */
    private SimWorld mySimWorld;
    /**
     * The WorldPanel used in the simulation.
     */
    private WorldPanel myWorldPanel;
    /**
     * The SimStatPanel used in the simulation.
     */
    private SimStatPanel mySimStatPanel;
    /**
     * The SimInputPanel used in the simulation.
     */
    private SimInputPanel mySimInputPanel;
    
    /**
     * Constructor for the ResetListener action listener.
     * @param aTime is the timer being used.
     * @param aSimWorld is the current SimWorld
     * @param aWorldPanel is the current WorldPanel.
     * @param aStatPanel is the current SimStatPanel.
     * @param aSimInputPanel is the current SimInputPanel.
     * @custom.post A StepListener is constructed.
     */
    public ResetListener(final Timer aTime, final SimWorld aSimWorld,
                        final WorldPanel aWorldPanel, final SimStatPanel aStatPanel, 
                        final SimInputPanel aSimInputPanel) {
        myTimer = aTime;
        mySimWorld = aSimWorld;
        myWorldPanel = aWorldPanel;
        mySimStatPanel = aStatPanel;
        mySimInputPanel = aSimInputPanel;
    }
    
    /**
     * Stops the timer, resets the simulation, and resets all stats.
     * @param anEvent is the Action event.
     * @custom.post the simulation is reset to new with values from the sliders.
     */
    @Override
    public void actionPerformed(ActionEvent anEvent) {
        myTimer.stop();
        mySimWorld.addHuman(mySimInputPanel.getMyPeopleSlider().getValue());
        mySimWorld.addBird(mySimInputPanel.getMyBirdSlider().getValue(),
                           mySimInputPanel.getMyInfectedSlider().getValue());
        myWorldPanel.setMyHumanMap(mySimWorld.getMyHumanMap());
        myWorldPanel.setMyBirdMap(mySimWorld.getMyBirdMap());
        mySimStatPanel.setMyDays(0);
        mySimStatPanel.setInfectedHumanText(mySimWorld.getInfectedHumans());
        mySimStatPanel.setHealthyHumanText(mySimWorld.getHealthyHumans());
        mySimStatPanel.setInfectedBirdText(mySimWorld.getInfectedBirds()); 
        mySimStatPanel.setHealthyBirdText(mySimWorld.getHealthyBirds());
        mySimStatPanel.setDayText(mySimStatPanel.getMyDays());
        myWorldPanel.repaint();
        
    }

}

