/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Action listener that ends the simulation when the stop button is pressed, 
 * gives the user the option to save results as a .csv file, and saves the results
 * if the user chooses to.
 * @author Christopher Helmer
 * @version 1
 *
 */
public class StopListener implements ActionListener {
    
    /**
     * Represents commas.
     */
    private final String myComma = ", ";
    /**
     * Timer used by the simulation.
     */
    private Timer myTimer;
    /**
     * SimWorld used by the simulation.
     */
    private SimWorld mySimWorld;
    /**
     * PrintStream used to save results.
     */
    private PrintStream myOutput;
    /**
     * FileChooser used to save results.
     */
    private JFileChooser myFilePicker;
    
    /**
     * Constructor for the StopListener class. 
     * @param aTimer is the timer used for the simulation.
     * @param aSimWorld is the SimWorld used for the simulation.
     * @custom.post A StopListener is generated.
     */
    public StopListener(final Timer aTimer, final SimWorld aSimWorld) {
        myTimer = aTimer;
        mySimWorld = aSimWorld;
    }
    
    /**
     * Stops the simulation, prompts user with a choice to save results as
     * a .csv file or not, and saves the results if the user chooses to.
     * @param anEvent is the event.
     */
    @Override
    public void actionPerformed(ActionEvent anEvent) {
        myTimer.stop();
        
        int response;
        response = JOptionPane.showConfirmDialog(null, "Would you like to "
                + "save the simulation results?");
        if (response == JOptionPane.YES_OPTION) {
            int select = -1;
            if (myFilePicker == null) {
                myFilePicker = new JFileChooser();
            }
            myFilePicker.resetChoosableFileFilters();
            myFilePicker.setFileFilter(new FileNameExtensionFilter(
                                       "CSV format (*.csv)", "csv"));
            select = myFilePicker.showSaveDialog(null);
            final File result;
            if (select == JFileChooser.APPROVE_OPTION) {
                result = myFilePicker.getSelectedFile();
                final String extension = myFilePicker.getFileFilter().
                        getDescription().substring(0, 3);
                saveText(extension, result.getPath());
            }
        }   
    }
    
    /**
     * Method used to save the simulation results to a .csv file.
     * @param aFileName is the name of file being saved.
     * @param aPath is the path of the file being saved. 
     * @custom.post The file is saved.
     */
    private void saveText(final String aFileName, String aPath) {
        final String path = aPath + "." + aFileName;
        try {
            final File report = new File(path);
            myOutput = new PrintStream(report);
            myOutput.println("Simulation Statistics");
            myOutput.println();
            myOutput.println("Humans, Healthy Birds, Sick Birds, "
                    + "Total Days, % Chance of Infection");
            myOutput.println(mySimWorld.getMyInitialHumanCount() + myComma 
                         + mySimWorld.getMyInitialHealthyBirdCount() + myComma 
                         + mySimWorld.getMyInitialSickBirdCount() + myComma 
                         + mySimWorld.getMyTotalDaysCount() + myComma
                         + mySimWorld.getMyPercentChanceOfInfection());
        } catch (final IOException e) {
            System.out.println("couldn't write a file");
        }
    }
}
