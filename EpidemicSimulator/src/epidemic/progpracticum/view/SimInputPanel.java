/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import epidemic.progpracticum.model.SimParams;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * Panel that allows the user to choose the number of people and birds as well
 * as the percentage of infected birds, and also has the button to generate the initial
 * world with humans, birds, and infected birds.
 * @author Christopher Helmer
 * @version 1
 *
 */
@SuppressWarnings("serial")
public class SimInputPanel extends JPanel {
    
    /**
     * Width of the human and bird sliders.
     */
    private final int myWidth1 = 400;
    /**
     * Width of the percent infected slider.
     */
    private final int myWidth2 = 200;
    /**
     * Height of the sliders.
     */
    private final int myHeight = 75;
    /**
     * String for the number of people slider.
     */
    private final String myPeople = "Select number of people: ";
    /**
     * Major tick spacing 200.
     */
    private final int myMajor200 = 200;
    /**
     * Minor tick spacing 50.
     */
    private final int myMinor50 = 50;
    /**
     * Major tick spacing 20.
     */
    private final int myMajor20 = 20;
    /**
     * Minor tick spacing 5.
     */
    private final int myMinor5 = 5;
    /**
     * String for number of bird slider.
     */
    private final String myBirds = "Select number of birds: ";
    /**
     * String for the percent sign.
     */
    private final String myPercent = "%";
    /**
     * String for the percent infected slider.
     */
    private final String myInfected = "Infected birds: ";
    
    
    /**
     * Represents the JSlider to select the number of people for the simulation.
     */
    private JSlider myPeopleSlider;
    /**
     * Represents the JSlider to select the number of birds for the simulation.
     */
    private JSlider myBirdSlider;
    /**
     * Represents the JSlider to select percentage of initially infected birds 
     * in the simulation.
     */
    private JSlider myInfectedSlider;
    /**
     * JButton used to populate the world with humans and birds.
     */
    private JButton myGenerateButton;
    /**
     * The SimWold used in the simulation.
     */
    private SimWorld mySimWorld;
    /**
     * The WorldPanel used in the simulation.
     */
    private WorldPanel myWorld;
    /**
     * The SimStatPanel used in the simulation.
     */
    private SimStatPanel myStatPanel;
    /**
     * Represents the number of humans in the simulation.
     */
    private int myHumanCount;
    /**
     * Represents the number of birds in the simulation.
     */
    private int myBirdCount;
    /**
     * Represents the percent of birds infected in the simulation.
     */
    private int myPctCount;
    /**
     * Dimension for the human and bird sliders.
     */
    private Dimension mySliderDimension1;
    /**
     * Dimension for the percent infected slider.
     */
    private Dimension mySliderDimension2;
    
    
    
    /**
     * The SimInputPanel Constructor.
     * @param aSim is the SimWorld object.
     * @param aWorld is the WorldPanel object.
     * @param aStatPanel is the SimStatPanel object.
     * @custom.post A new SimInputPanel object is generated.
     */
    public SimInputPanel(final SimWorld aSim, 
                         final WorldPanel aWorld, 
                         final SimStatPanel aStatPanel) {
        
        myWorld = aWorld;
        mySimWorld = aSim;
        myStatPanel = aStatPanel;
        mySliderDimension1 = new Dimension(myWidth1, myHeight);
        mySliderDimension2 = new Dimension(myWidth2, myHeight);
        simInputSetup();
    }
    
    /**
     * Helper method for the constructor that runs the humanSliderSetup, birdSliderSetup,
     * and infectedSliderSetup methods, creates a button to generate the initial instance
     * of the painted WorldPanel, and adds the sliders and buttons to the panel.
     */
    private void simInputSetup() {
        humanSliderSetup();
        birdSliderSetup();
        infectedSliderSetup();
        
        myGenerateButton = new JButton("Generate World");
        final GenerateListener generate = new GenerateListener(mySimWorld, myWorld,
                                                               myStatPanel,
                                                               myHumanCount,
                                                               myBirdCount, 
                                                               myPctCount);
        myGenerateButton.addActionListener(generate);
        add(myPeopleSlider);
        add(myBirdSlider);
        add(myInfectedSlider);
        add(myGenerateButton); 
    }
    
    /**
     * Helper method for constructor. Creates a slider to choose the number of 
     * people for the simulation and attaches a change listener.
     */
    private void humanSliderSetup() {
        myPeopleSlider = new JSlider(0, SimParams.MAX_HUMAN_NUM, 0);
        myPeopleSlider.setPreferredSize(mySliderDimension1);
        myPeopleSlider.setToolTipText("" + myPeopleSlider.getValue());
        myPeopleSlider.setBorder(BorderFactory.
                                 createTitledBorder(myPeople + myPeopleSlider.getValue()));
        
        myPeopleSlider.addChangeListener(new ChangeListener() {
            
            /**
             * Sets the tool text tip for the slider to the value as it slides, 
             * updates the label to show the number the slider is at, populates the 
             * human map in mySimWorld and sets the human map in WorldPanel to that 
             * map.
             */
            @Override
            public void stateChanged(ChangeEvent anEvent) {
                myPeopleSlider.setToolTipText("" + myPeopleSlider.getValue());
                myPeopleSlider.setBorder(BorderFactory.
                                 createTitledBorder(myPeople + myPeopleSlider.getValue()));
                if (!myPeopleSlider.getValueIsAdjusting()) {
                    myHumanCount = myPeopleSlider.getValue();
                    mySimWorld.addHuman(myPeopleSlider.getValue());
                }
                myWorld.setMyHumanMap(mySimWorld.getMyHumanMap());
            } 
        });
        
        myPeopleSlider.setMajorTickSpacing(myMajor200);
        myPeopleSlider.setMinorTickSpacing(myMinor50);
        
        myPeopleSlider.setPaintTicks(true);
        myPeopleSlider.setPaintLabels(true);
    }
    
    /**
     * Helper method for constructor. Creates a slider to choose the number of 
     * birds for the simulation and attaches a change listener.
     */
    private void birdSliderSetup() {
        myBirdSlider = new JSlider(0, SimParams.MAX_BIRD_NUM, 0);
        myBirdSlider.setToolTipText("" + myBirdSlider.getValue());
        myBirdSlider.setPreferredSize(mySliderDimension1);
        myBirdSlider.setBorder(BorderFactory.
                   createTitledBorder(myBirds + myBirdSlider.getValue()));
        
        myBirdSlider.addChangeListener(new ChangeListener() {
            
            /**
             * Sets the tool text tip for the slider to the value as it slides, 
             * updates the label to show the number the slider is at, populates the 
             * bird map in mySimWorld and sets the bird map in WorldPanel to that 
             * map.
             */
            @Override
            public void stateChanged(ChangeEvent anEvent) {
                myBirdSlider.setToolTipText("" + myBirdSlider.getValue());
                myBirdSlider.setBorder(BorderFactory.
                       createTitledBorder(myBirds + myBirdSlider.getValue()));
                if (!myBirdSlider.getValueIsAdjusting()) {
                    myBirdCount = myBirdSlider.getValue();
                    mySimWorld.addBird(myBirdSlider.getValue(), myInfectedSlider.getValue());
                }
                myWorld.setMyBirdMap(mySimWorld.getMyBirdMap());
            }
        });
        
        myBirdSlider.setMajorTickSpacing(myMajor200);
        myBirdSlider.setMinorTickSpacing(myMinor50);
        
        myBirdSlider.setPaintTicks(true);
        myBirdSlider.setPaintLabels(true);
    }
    
    /**
     * Helper method for constructor. Creates a slider to choose the percent of 
     * infected birds for the simulation and attaches a change listener.
     */
    private void infectedSliderSetup() {
        myInfectedSlider = new JSlider(0, SimParams.ANIMATION_STEP_TIME, 0);
        myInfectedSlider.setToolTipText("" + myInfectedSlider.getValue() + myPercent);
        myInfectedSlider.setPreferredSize(mySliderDimension2);
        myInfectedSlider.setBorder(BorderFactory.
                   createTitledBorder(myInfected + myInfectedSlider.getValue() + myPercent));
        
        myInfectedSlider.addChangeListener(new ChangeListener() {
            
            /**
             * Sets the tool text tip for the slider to the value as it slides, 
             * updates the label to show the number the slider is at, populates the 
             * bird map in mySimWorld and sets the bird map in WorldPanel to that 
             * map.
             */
            @Override
            public void stateChanged(ChangeEvent anEvent) {
                myInfectedSlider.setToolTipText("" + myInfectedSlider.getValue() + myPercent);
                myInfectedSlider.setBorder(BorderFactory.
                     createTitledBorder(myInfected + myInfectedSlider.getValue() + myPercent));
                if (!myInfectedSlider.getValueIsAdjusting()) {
                    myPctCount = myInfectedSlider.getValue();
                    mySimWorld.addBird(myBirdSlider.getValue(), myInfectedSlider.getValue());
                }
                myWorld.setMyBirdMap(mySimWorld.getMyBirdMap());
            } 
        });
        
        myInfectedSlider.setMajorTickSpacing(myMajor20);
        myInfectedSlider.setMinorTickSpacing(myMinor5);
        
        myInfectedSlider.setPaintTicks(true);
        myInfectedSlider.setPaintLabels(true);
    } 
    
    /**
     * Gets the JSlider that represents the number of humans in the simulation.
     * @return the JSlider that represents the number of humans in the simulation.
     */
    public JSlider getMyPeopleSlider() {
        return myPeopleSlider;
    }
    
    /**
     * Gets the JSlider that represents the number of birds in the simulation.
     * @return the JSlider that represents the number of birds in the simulation.
     */
    public JSlider getMyBirdSlider() {
        return myBirdSlider;
    }
    
    /**
     * Gets the JSlider that represents the percentage of birds infected in the simulation.
     * @return the JSlider that represents the percentage of birds infected in the simulation.
     */
    public JSlider getMyInfectedSlider() {
        return myInfectedSlider;
    }
}
