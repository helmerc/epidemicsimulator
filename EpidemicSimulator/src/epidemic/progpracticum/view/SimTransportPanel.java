/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import epidemic.progpracticum.model.SimParams;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;



/**
 * Panel that contains all of the transport controls (start, stop, step, and reset)  
 * that control the simulation.
 * @author Christopher Helmer   
 * @version 1
 *
 */
@SuppressWarnings("serial")
public class SimTransportPanel extends JPanel {
    
    /**
     * JButton for starting the simulation.
     */
    private JButton myStartButton;
    /**
     * JButton for stopping the simulation.
     */
    private JButton myStopButton;
    /**
     * JButton for stepping through the simulation.
     */
    private JButton myStepButton;
    /**
     * JButton for resetting the simulation.
     */
    private JButton myResetButton;
    /**
     * Represents the current SimWorld.
     */
    private SimWorld mySimWorld;
    /**
     * Represents the current WorldPanel.
     */
    private WorldPanel myWorldPanel;
    /**
     * Represents the current SimStatPanel.
     */
    private SimStatPanel mySimStatPanel;
    /**
     * Represents the current SimInputPanel.
     */
    private SimInputPanel mySimInputPanel;
    /**
     * Action listener used by the timer.
     */
    private MoveListener myMove;
    /**
     * Timer for continuous running of the simulation.
     */
    private Timer myTimer;
    /**
     * Listener to stop the timer at the end of the simulation
     * and prompt to save.
     */
    private EndListener myEndListener;
    /**
     * Listener to stop the simulation and prompt to save.
     */
    private StopListener myStopListener;
    
    /**
     * Constructor for the SimTransportPanel.
     * @param aSimWorld is the current SimWorld.
     * @param aWorldPanel is the current WorldPanel.
     * @param aSimStatPanel is the current SimStatPanel.
     * @param aSimInputPanel is the current SimInputPanel.
     * @custom.post The SimTransportPanel is generated.
     */
    public SimTransportPanel(final SimWorld aSimWorld, 
                             final WorldPanel aWorldPanel, 
                             final SimStatPanel aSimStatPanel,
                             final SimInputPanel aSimInputPanel) {
        
        mySimWorld = aSimWorld;
        myWorldPanel = aWorldPanel;
        mySimStatPanel = aSimStatPanel;
        mySimInputPanel = aSimInputPanel;
        myMove = new MoveListener(aSimWorld, aWorldPanel, aSimStatPanel);
        myTimer = new Timer(SimParams.ANIMATION_STEP_TIME, myMove);
        myEndListener = new EndListener(myTimer, mySimWorld);
        myStopListener = new StopListener(myTimer, mySimWorld);
        simTransportSetup();
    }
    
    /**
     * Helper method for the constructor that adds Start, Stop, Step, 
     * and reset buttons with their action listeners to the panel. Also 
     * adds an action listener to the timer so that it will stop when 
     * all birds and humans are infected.
     */
    private void simTransportSetup() {
        myStartButton = new JButton("Start");
        myStopButton = new JButton("Stop");
        myStepButton = new JButton("Step");
        myResetButton = new JButton("Reset");
        
        myStartButton.addActionListener(new ActionListener() {
            /**
             * Starts the timer for the simulation.
             */
            @Override
            public void actionPerformed(ActionEvent anEvent) {
                myTimer.start();   
            } 
        });
        
        myStopButton.addActionListener(myStopListener);
        
        myResetButton.addActionListener(new ResetListener(myTimer, mySimWorld,
                                                         myWorldPanel, mySimStatPanel,
                                                         mySimInputPanel));
        
        myStepButton.addActionListener(myMove);
        
        myTimer.addActionListener(myEndListener);
        
        add(myStartButton);
        add(myStopButton);
        add(myStepButton);
        add(myResetButton);
    }
    

}
