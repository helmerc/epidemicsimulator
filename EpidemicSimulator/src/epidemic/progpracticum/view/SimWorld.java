/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import epidemic.progpracticum.model.Bird;
import epidemic.progpracticum.model.Human;
import epidemic.progpracticum.model.SimParams;

import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RectangularShape;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class that generates, sets, and stores the maps containing human and bird information 
 * and grants access to the number of humans and birds that are sick.
 * @author Christopher Helmer   
 * @version 1
 *
 */
public class SimWorld {
    
    /**
     * Double used to calculate percentage.
     */
    private final double myPctCalc = 0.01;
    /**
     * Map that stores the location of human and a human object.
     */
    private Map<RectangularShape, Human> myHumanMap;
    /**
     * Map that stores the location of bird and a bird object.
     */
    private Map<RectangularShape, Bird> myBirdMap;
    /**
     * Represents a set of Rectangular shapes.
     */
    private Set<RectangularShape> myHuman;
    /**
     * Represents a set of Rectangular shapes.
     */
    private Set<RectangularShape> myBird;
    /**
     * Represents the initial human map generated when the generate button is pressed.
     */
    private Map<RectangularShape, Human> myInitialHumanMap;
    /**
     * Represents the initial bird map generated when the generate button is pressed.
     */
    private Map<RectangularShape, Bird> myInitialBirdMap;
    /**
     * Represents the initial number of humans in the simulation.
     */
    private int myInitialHumanCount;
    /**
     * Represents the initial number of healthy birds in the simulation.  
     */
    private int myInitialHealthyBirdCount;
    /**
     * Represents the initial number of sick birds in the simulation.
     */
    private int myInitialSickBirdCount;
    /**
     * Represents the total number of days the simulation ran.
     */
    private int myTotalDaysCount;
    
    /**
     * Constructor for the SimWorld.
     */
    public SimWorld() {
        myHumanMap = new HashMap<RectangularShape, Human>();
        myBirdMap = new HashMap<RectangularShape, Bird>();    
    }
    
    /**
     * Clears the current map, sets the initial number of humans, and adds 
     * the ellipses and humans to the human map.
     * @param aNumHum is the number of humans selected for the simulation.
     * @custom.post All of the ellipses humans are added to the human map.
     */
    public void addHuman(int aNumHum) {
        myHumanMap.clear();
        this.setMyInitialHumanCount(aNumHum);
        while (myHumanMap.size() < aNumHum) {
            final Point p = new Point(SimParams.GENERATOR.nextInt(SimParams.PANEL_PIX_WIDTH), 
                                      SimParams.GENERATOR.nextInt(SimParams.PANEL_PIX_HEIGHT));
            
            final RectangularShape human = 
                    new Ellipse2D.Double(p.x, p.y, SimParams.HUMAN_DIM, SimParams.HUMAN_DIM);
            if (human.getFrame().getMaxX() > SimParams.PANEL_PIX_WIDTH) {
                human.setFrame(0, p.getY(), SimParams.HUMAN_DIM, SimParams.HUMAN_DIM);
            }
            if (human.getFrame().getMinX() < 0) {
                human.setFrame(SimParams.PANEL_PIX_WIDTH, p.getY(), 
                               SimParams.HUMAN_DIM, SimParams.HUMAN_DIM);
            }
            if (human.getFrame().getMaxY() > SimParams.PANEL_PIX_HEIGHT) {
                human.setFrame(p.getX(), 0, SimParams.HUMAN_DIM, SimParams.HUMAN_DIM);
            }
            if (human.getFrame().getMinY() < 0) {
                human.setFrame(p.getX(), SimParams.PANEL_PIX_HEIGHT, 
                               SimParams.HUMAN_DIM, SimParams.HUMAN_DIM);
            }
            
            boolean isNotHuman = true;
            boolean isNotBird = true;
            
            myHuman = myHumanMap.keySet();
            myBird = myBirdMap.keySet();
            
        inner: 
            for (RectangularShape h : myHuman) {
                if (human.getFrame().intersects(h.getFrame())) {
                    isNotHuman = false;
                    break inner;
                }
            }
        inner: 
            for (RectangularShape b : myBird) {
                if (human.getFrame().intersects(b.getFrame())) {
                    isNotBird = false;
                    break inner;
                }
            }
            if (isNotHuman && isNotBird) {
                myHumanMap.put(human, new Human('H', false, 0, p));
            }
        }
    }
    
    /**
     * Clears the current map and adds the ellipses and birds to the bird map.
     * @param aNumBird is the number of birds selected for the simulation.
     * @param aPct is the percent of infected birds in the simulation.
     * @custom.post All the ellipses and birds are added to the bird map.
     */
    public void addBird(int aNumBird, int aPct) {
        myBirdMap.clear();
        
        calculateInfected(aNumBird, aPct);
    }
    
    /**
     * Helper method for the addBird method that calculates how many infected 
     * birds there are out of the total number of birds, sets the intitial number
     * of healthy and sick birds, and calls the mapBirds and 
     * mapInfectedBirds methods with the given number of birds.
     * @param aNumBird is the total number of birds.
     * @param aPct is the percentage of birds infected.
     * @custom.post the number of infected birds is calculated and the other 
     * helper methods are called.
     */
    private void calculateInfected(int aNumBird, int aPct) {
        double percent = (double) aPct;
        double bird = (double) aNumBird;
        
        percent *= myPctCalc;
        bird *= percent;
        
        final int infected = (int) bird;
        final int healthy = aNumBird - infected;
        this.setMyInitialSickBirdCount(infected);
        this.setMyInitialHealthyBirdCount(healthy);
        mapBirds(healthy);
        mapInfectedBirds(infected);
    }
    
    /**
     * Helper method for the addBird method that adds the healthy birds 
     * and ellipses to the bird map.
     * @param aHealthy is the number of healthy birds in the simulation.
     * @custom.post the ellipses and healthy birds are added to the bird map.
     */
    private void mapBirds(int aHealthy) {
        while (myBirdMap.size() < aHealthy) {
            final Point p = new Point(SimParams.GENERATOR.nextInt(SimParams.PANEL_PIX_WIDTH), 
                                      SimParams.GENERATOR.nextInt(SimParams.PANEL_PIX_HEIGHT));
            
            final RectangularShape birdShape = 
                    new Ellipse2D.Double(p.x, p.y, SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            
            if (birdShape.getFrame().getMaxX() > SimParams.PANEL_PIX_WIDTH) {
                birdShape.setFrame(0, p.getY(), SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            if (birdShape.getFrame().getMinX() < 0) {
                birdShape.setFrame(SimParams.PANEL_PIX_WIDTH, p.getY(), 
                               SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            if (birdShape.getFrame().getMaxY() > SimParams.PANEL_PIX_HEIGHT) {
                birdShape.setFrame(p.getX(), 0, SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            if (birdShape.getFrame().getMinY() < 0) {
                birdShape.setFrame(p.getX(), SimParams.PANEL_PIX_HEIGHT, 
                               SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            
            boolean isNotHuman = true;
            boolean isNotBird = true;
            
            myHuman = myHumanMap.keySet();
            myBird = myBirdMap.keySet();
            
        inner: 
            for (RectangularShape h : myHuman) {
                if (birdShape.getFrame().intersects(h.getFrame())) {
                    isNotHuman = false;
                    break inner;
                }
            }
        inner: 
            for (RectangularShape b : myBird) {
                if (birdShape.getFrame().intersects(b.getFrame())) {
                    isNotBird = false;
                    break inner;
                }
            }
            if (isNotHuman && isNotBird) {
                myBirdMap.put(birdShape, new Bird('B', false, 0, p));
            }   
        }
    }
    
    /**
     * Helper method for the addBird method that adds the ellipses 
     * and infected birds to the bird map.
     * @param anInfected is the number of infected birds in the simulation.
     * @custom.post the ellipses and infected birds are added to the bird map.
     */
    private void mapInfectedBirds(int anInfected) {
        final Map<RectangularShape, Bird> infect = 
                new HashMap<RectangularShape, Bird>();
        
        while (infect.size() < anInfected) {
            final Point p = new Point(SimParams.GENERATOR.nextInt(SimParams.PANEL_PIX_WIDTH),
                                      SimParams.GENERATOR.nextInt(SimParams.PANEL_PIX_HEIGHT));
            
            final RectangularShape birdShape = 
                    new Ellipse2D.Double(p.x, p.y, SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            
            if (birdShape.getFrame().getMaxX() > SimParams.PANEL_PIX_WIDTH) {
                birdShape.setFrame(0, p.getY(), SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            if (birdShape.getFrame().getMinX() < 0) {
                birdShape.setFrame(SimParams.PANEL_PIX_WIDTH, p.getY(), 
                               SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            if (birdShape.getFrame().getMaxY() > SimParams.PANEL_PIX_HEIGHT) {
                birdShape.setFrame(p.getX(), 0, SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            if (birdShape.getFrame().getMinY() < 0) {
                birdShape.setFrame(p.getX(), SimParams.PANEL_PIX_HEIGHT, 
                               SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            }
            
            boolean isNotHuman = true;
            boolean isNotBird = true;
            
            myHuman = myHumanMap.keySet();
            myBird = myBirdMap.keySet();
            
        inner: 
            for (RectangularShape h : myHuman) {
                if (birdShape.getFrame().intersects(h.getFrame())) {
                    isNotHuman = false;
                    break inner;
                }
            }
        inner: 
            for (RectangularShape b : myBird) {
                if (birdShape.getFrame().intersects(b.getFrame())) {
                    isNotBird = false;
                    break inner;
                }
            }
            if (isNotHuman && isNotBird) {
                infect.put(birdShape, new Bird('B', true, 0, p));
            }
        }
        myBirdMap.putAll(infect);
    }
    
    /**
     * Gets the map of ellipses and humans.
     * @return the map of ellipses and humans.
     */
    public Map<RectangularShape, Human> getMyHumanMap() {
        return myHumanMap;
    }
    
    /**
     * Sets the myHumanMap to the input map.
     * @param aMap is the input map.
     * @custom.post myHumanMap is changed to the input map.
     */
    public void setMyHumanMap(final Map<RectangularShape, Human> aMap) {
        myHumanMap = aMap;
    }
    
    /**
     * Gets the map of ellipses and birds.
     * @return the map of ellipses to birds.
     */
    public Map<RectangularShape, Bird> getMyBirdMap() {
        return myBirdMap;
    }
    
    /**
     * Sets myBirdMap to the input map.
     * @param aMap is the input map.
     * @custom.post myBirdMap is set to the input map.
     */
    public void setMyBirdMap(final Map<RectangularShape, Bird> aMap) {
        myBirdMap = aMap;
    }
    
    /**
     * Gets the number of healthy humans.
     * @return the number of healthy humans.
     */
    public int getHealthyHumans() {
        int total = 0;
        try {
            myHuman = myHumanMap.keySet();
            for (RectangularShape h : myHuman) {
                if (!myHumanMap.get(h).isSick()) {
                    total++;
                }
            }  
        } catch (final NullPointerException e) {
            System.out.println("");
        }
        return total;
    }
    
    /**
     * Gets the number of infected humans.
     * @return the number of infected humans.
     */
    public int getInfectedHumans() {
        int total = 0;
        try {
            myHuman = myHumanMap.keySet();
            for (RectangularShape h : myHuman) {
                if (myHumanMap.get(h).isSick()) {
                    total++;
                }
            }
        } catch (final NullPointerException e) {
            System.out.println("");
        }
        return total;
    }
    
    /**
     * Gets the number of healthy birds. 
     * @return the number of healthy birds.
     */
    public int getHealthyBirds() {
        int total = 0;
        
        try {
            myBird = myBirdMap.keySet();
            for (RectangularShape b : myBird) {
                if (!myBirdMap.get(b).isSick()) {
                    total++;
                }
            }
        } catch (final NullPointerException e) {
            System.out.println("");
        }
        return total;
    }
    
    /** 
     * Gets the number of infected birds.
     * @return the number of infected birds.
     */
    public int getInfectedBirds() {
        int total = 0;
        try {
            myBird = myBirdMap.keySet();
            for (RectangularShape b : myBird) {
                if (myBirdMap.get(b).isSick()) {
                    total++;
                }
            } 
        } catch (final NullPointerException e) {
            System.out.println("");
        }
        return total;
    }
    
    /**
     * Gets the initial human map generated when the generate button is pressed.
     * @return the initial human map. 
     */
    public Map<RectangularShape, Human> getMyInitialHumanMap() {
        return myInitialHumanMap;
    }
    
    /**
     * Sets the initial human map to the input map.
     * @param aMap is a map of ellipses to humans. 
     * @custom.post myInitialHumanMap is set to the input map.
     */
    public void setMyInitialHumanMap(Map<RectangularShape, Human> aMap) {
        myInitialHumanMap = aMap;
    }
    
    /**
     * Gets the initial bird map generated when the generate button is pressed.
     * @return the initial bird map. 
     */
    public Map<RectangularShape, Bird> getMyInitialBirdMap() {
        return myInitialBirdMap;
    }
    
    /**
     * Sets the initial bird map to the input map.
     * @param aMap is a map of ellipses to birds. 
     * @custom.post myInitialBirdMap is set to the input map.
     */
    public void setMyInitialBirdMap(Map<RectangularShape, Bird> aMap) {
        myInitialBirdMap = aMap;
    }
    
    /**
     * Gets the initial human count.
     * @return the initial human count.
     */
    public int getMyInitialHumanCount() {
        return myInitialHumanCount;
    }
    
    /**
     * Sets the initial human count.
     * @param aCount is the initial number of humans chosen for the simulation.
     * @custom.post The initial number of humans is set.
     */
    public void setMyInitialHumanCount(final int aCount) {
        myInitialHumanCount = aCount;
    }
    
    /**
     * Gets the initial healthy bird count.
     * @return the initial healthy bird count.
     */
    public int getMyInitialHealthyBirdCount() {
        return myInitialHealthyBirdCount;
    }
    
    /**
     * Sets the initial healthy bird count.
     * @param aCount is the initial number of healthy birds chosen for the simulation.
     * @custom.post The number of healthy birds is set.
     */
    public void setMyInitialHealthyBirdCount(final int aCount) {
        myInitialHealthyBirdCount = aCount;
    }
    
    /**
     * Gets the initial sick bird count.
     * @return the initial sick bird count.
     */
    public int getMyInitialSickBirdCount() {
        return myInitialSickBirdCount;
    }
    
    /**
     * Sets the initial sick bird count.
     * @param aCount is the initial number of sick birds chosen for the simulation.
     * @custom.post The number of sick birds is set.
     */
    public void setMyInitialSickBirdCount(final int aCount) {
        myInitialSickBirdCount = aCount;
    }
    
    /**
     * Gets the total days of the simulation.
     * @return the total number of days of the simulation.
     */
    public int getMyTotalDaysCount() {
        return myTotalDaysCount;
    }
    
    /**
     * Sets the total days of the simulation.
     * @param aCount is the number of days.
     * @custom.post The total number of days is set.
     */
    public void setMyTotalDaysCount(final int aCount) {
        myTotalDaysCount = aCount;
    }
    
    /**
     * Gets the percent chance of infection.
     * @return the percent chance of infection.
     */
    public double getMyPercentChanceOfInfection() {
        return SimParams.CHANCE_OF_INFECTION * SimParams.ANIMATION_STEP_TIME;
    }
}
