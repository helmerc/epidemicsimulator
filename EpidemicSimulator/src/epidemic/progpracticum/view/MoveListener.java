/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import epidemic.progpracticum.model.Bird;
import epidemic.progpracticum.model.Human;
import epidemic.progpracticum.model.SimParams;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RectangularShape;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;



/**
 * Action Listener class that makes the movements for human and bird objects
 * and makes sick those  who come into contact with those who are sick 
 * SimParams.CHANCE_OF_INFECTION * 100 percent of the time.
 * Note: Used by the timer and the step button. 
 * @author Christopher Helmer
 * @version 1
 *
 */
public class MoveListener implements ActionListener {
    
    /**
     * Represents the SimInputPanel affected by this listener.
     */
    private SimStatPanel myStatPanel;
    /**
     * Represents the SimWorld affected by this listener.
     */
    private SimWorld mySimWorld;
    /**
     * Represents the WorldPanel affected by this listener.
     */
    private WorldPanel myWorld;
    /**
     * Represents the map of humans and ellipses.
     */
    private Map<RectangularShape, Human> myHumanMap;
    /**
     * Represents the map of birds and ellipses.
     */
    private Map<RectangularShape, Bird> myBirdMap;
    /**
     * Temporary map of humans and ellipses.
     */
    private Map<RectangularShape, Human> myTempHumanMap1;
    /**
     * Temporary map of birds and ellipses.
     */
    private Map<RectangularShape, Bird> myTempBirdMap1;
    /**
     * Temporary map of humans and ellipses.
     */
    private Map<RectangularShape, Human> myTempHumanMap2;
    /**
     * Temporary map of birds and ellipses.
     */
    private Map<RectangularShape, Bird> myTempBirdMap2;
    /**
     * Temporary map of humans and ellipses.
     */
    private Map<RectangularShape, Human> myTempHumanMap3;
    /**
     * Temporary map of birds and ellipses.
     */
    private Map<RectangularShape, Bird> myTempBirdMap3;
    /**
     * Integer that represents how many days the simulation runs for.
     */
    private int myTimeKeeper;
    
    /**
     * Generates a new MoveListener object.
     * @param aSimWorld is the SimWorld used.
     * @param aWorldPanel is the WorldPanel used.
     * @param aSimStatPanel is the SimInputPanel used.
     */
    public MoveListener(final SimWorld aSimWorld, 
                        final WorldPanel aWorldPanel, 
                        final SimStatPanel aSimStatPanel) {
        mySimWorld = aSimWorld;
        myWorld = aWorldPanel;
        myStatPanel = aSimStatPanel;
    }
    
    /**
     * Moves the humans and birds through the world, makes sick those 
     * who come into contact with those who are sick 
     * SimParams.CHANCE_OF_INFECTION * 100 percent of the time, updates the 
     * SimStatPanel, and repaints the WorldPanel.
     * @param anEvent is the event.
     */
    @Override
    public void actionPerformed(ActionEvent anEvent) {
        moveHumans();
        moveBirds();
        makeSick();
        
        myHumanMap = myTempHumanMap3;
        myBirdMap = myTempBirdMap3;
        
        myWorld.setMyHumanMap(myHumanMap);
        myWorld.setMyBirdMap(myBirdMap);
        
        mySimWorld.setMyHumanMap(myHumanMap);
        mySimWorld.setMyBirdMap(myBirdMap);
        myTimeKeeper = myStatPanel.getMyDays() + 1;
        mySimWorld.setMyTotalDaysCount(myTimeKeeper);
        myStatPanel.setMyDays(myTimeKeeper);
        myStatPanel.setInfectedHumanText(mySimWorld.getInfectedHumans());
        myStatPanel.setHealthyHumanText(mySimWorld.getHealthyHumans());
        myStatPanel.setInfectedBirdText(mySimWorld.getInfectedBirds()); 
        myStatPanel.setHealthyBirdText(mySimWorld.getHealthyBirds());
        myStatPanel.setDayText(myStatPanel.getMyDays());
        
        
        myWorld.repaint();     
    }
    
    /**
     * Helper method that creates temporary ellipses and humans, moves their
     * position, and adds them to a temporary map.
     */
    private void moveHumans() {
        myHumanMap = mySimWorld.getMyHumanMap();
        
        final Set<RectangularShape> humanSet1 = myHumanMap.keySet();
        myTempHumanMap1 = new HashMap<RectangularShape, Human>();
        for (RectangularShape h1 : humanSet1) {
            final Human human = myHumanMap.get(h1);
            final RectangularShape shape = h1;
            human.move();
            shape.setFrame(human.getMyLocation().getX(), 
                           human.getMyLocation().getY(), 
                           SimParams.HUMAN_DIM, SimParams.HUMAN_DIM);
        
            myTempHumanMap1.put(shape, human);   
        }
    }
    
    /**
     * Helper method that creates temporary ellipses and birds, moves their
     * position, and adds them to a temporary map.
     */
    private void moveBirds() {
        myBirdMap = mySimWorld.getMyBirdMap();
        
        final Set<RectangularShape> birdSet1 = myBirdMap.keySet();
        myTempBirdMap1 = new HashMap<RectangularShape, Bird>();
        for (RectangularShape b : birdSet1) {
            final Bird bird = myBirdMap.get(b);
            final RectangularShape shape = b;
            bird.move();
            shape.setFrame(bird.getMyLocation().getX(), 
                           bird.getMyLocation().getY(), 
                           SimParams.BIRD_DIM, SimParams.BIRD_DIM);
            
            myTempBirdMap1.put(shape, bird);
        }
    }
    
    /**
     * Helper method that compares the health of humans to humans, humans to birds, and birds
     * to birds and makes sick those who come into contact with those who are sick 
     * SimParams.CHANCE_OF_INFECTION * 100 percent of the time.
     */
    private void makeSick() {
        final Set<RectangularShape> humanSet2 = myTempHumanMap1.keySet();
        final Set<RectangularShape> birdSet2 = myTempBirdMap1.keySet();
        myTempHumanMap2 = new HashMap<RectangularShape, Human>();
        myTempBirdMap2 = new HashMap<RectangularShape, Bird>();
        for (RectangularShape h : humanSet2) {
            final Human human = myTempHumanMap1.get(h);
            final RectangularShape humanShape = h;
            for (RectangularShape b : birdSet2) {
                final Bird bird = myTempBirdMap1.get(b);
                final RectangularShape birdShape = b;
                final float chance = SimParams.GENERATOR.nextFloat();
                if (humanShape.getFrame().intersects(birdShape.getFrame()) 
                        && human.compareTo(bird) == 1 
                        && chance <= (float) SimParams.CHANCE_OF_INFECTION) {
                    
                    human.setMyIsSick(true);
                } 
                if (humanShape.getFrame().intersects(birdShape.getFrame()) 
                        && human.compareTo(bird) == -1
                        && chance <= (float) SimParams.CHANCE_OF_INFECTION) {
                    bird.setMyIsSick(true);
                }
                myTempBirdMap2.put(birdShape, bird);
            }
            myTempHumanMap2.put(humanShape, human);
        }
        
        final Set<RectangularShape> humanSet3 = myTempHumanMap2.keySet();
        myTempHumanMap3 = new HashMap<RectangularShape, Human>();
        for (RectangularShape h1 : humanSet2) {
            final Human human1 = myTempHumanMap1.get(h1);
            final RectangularShape shape1 = h1;
            for (RectangularShape h2 : humanSet3) {
                final Human human2 = myTempHumanMap2.get(h2);
                final RectangularShape shape2 = h2;
                final float chance = SimParams.GENERATOR.nextFloat();
                if (shape1.getFrame().intersects(shape2.getFrame()) 
                        && human1.compareTo(human2) == -1 
                        && chance <= (float) SimParams.CHANCE_OF_INFECTION) {
                    
                    human2.setMyIsSick(true);
                }
                myTempHumanMap3.put(shape2, human2);
            }
        }
        
        final Set<RectangularShape> birdSet3 = myTempBirdMap2.keySet();
        myTempBirdMap3 = new HashMap<RectangularShape, Bird>();
        for (RectangularShape b1 : birdSet2) {
            final Bird bird1 = myTempBirdMap1.get(b1);
            final RectangularShape shape1 = b1;
            for (RectangularShape b2 : birdSet3) {
                final Bird bird2 = myTempBirdMap2.get(b2);
                final RectangularShape shape2 = b2;
                final float chance = SimParams.GENERATOR.nextFloat();
                if (shape1.getFrame().intersects(shape2.getFrame()) 
                        && bird1.compareTo(bird2) == -1
                        && chance <= (float) SimParams.CHANCE_OF_INFECTION) {
                    
                    bird2.setMyIsSick(true);
                }
                myTempBirdMap3.put(shape2, bird2);
            }
        }
    }
}
