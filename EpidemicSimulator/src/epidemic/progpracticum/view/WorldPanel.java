/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import epidemic.progpracticum.model.Bird;
import epidemic.progpracticum.model.Human;
import epidemic.progpracticum.model.SimParams; 

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.RectangularShape;
import java.util.Map;
import java.util.Set;

import javax.swing.JPanel;

/**
 * The JPanel that displays humans and birds as ellipses for the simulation.
 * @author Christopher Helmer   
 * @version 1
 * @custom.inv myHumanMap and myBirdMap should not be empty.
 *
 */
@SuppressWarnings("serial")
public class WorldPanel extends JPanel {
    
    /**
     * The dimension of the WorldPanel.
     */
    private Dimension myPreferredSize;
    /**
     * A map that maps points to Human objects.
     */
    private Map<RectangularShape, Human> myHumanMap;
    /**
     * A map that maps points to Bird objects.
     */
    private Map<RectangularShape, Bird> myBirdMap;
    
    /**
     * Constructor for the WorldPanel.
     * @custom.post A WorldPanel object is created.
     */
    public WorldPanel() {
        super();
        myPreferredSize = new Dimension(SimParams.PANEL_PIX_WIDTH, SimParams.PANEL_PIX_HEIGHT);
        setPreferredSize(myPreferredSize);
        setBackground(Color.WHITE);
    }
    
    /**
     * Sets myHumanMap to the input map.
     * @param aMap is the map to set myHumanMap to.
     * @custom.post myHumanMap becomes the input map.
     */
    public void setMyHumanMap(final Map<RectangularShape, Human> aMap) {
        myHumanMap = aMap;
    }
    
    /**
     * Sets myBirdMap to the input map.
     * @param aMap is the map to set myBirdMap to.
     * @custom.post myBirdMap becomes the input map.
     */
    public void setMyBirdMap(final Map<RectangularShape, Bird> aMap) {
        myBirdMap = aMap;
    }
    
    /**
     * Paints ellipses representing humans and birds onto the panel.
     * @param aGraphic is the graphic used.
     * @custom.post Ellipses representing humans and birds are drawn onto 
     * the panel for the simulation.
     */
    @Override
    public void paintComponent(final Graphics aGraphic) {
        super.paintComponent(aGraphic);
        final Graphics2D g2d = (Graphics2D) aGraphic;
        
        try {
            final Set<RectangularShape> human_points = myHumanMap.keySet();
            for (RectangularShape h : human_points) {
                if (myHumanMap.get(h).isSick()) {
                    g2d.setPaint(Color.RED);
                } else {
                    g2d.setPaint(Color.GREEN);
                }
                g2d.fill(h);
            }
        } catch (final NullPointerException e) {
            System.out.println("");
        }
        
        try {
            final Set<RectangularShape> human_points = myBirdMap.keySet();
            for (RectangularShape b : human_points) {
                if (myBirdMap.get(b).isSick()) {
                    g2d.setPaint(Color.ORANGE);
                } else {
                    g2d.setPaint(Color.GRAY);
                }
                g2d.fill(b);
            }
        } catch (final NullPointerException e) {
            System.out.println("");
        }
        
    }

}
