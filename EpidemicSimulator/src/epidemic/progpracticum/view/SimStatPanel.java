/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Panel that has a real-time display of the number of healthy and infected
 * humans and birds and the number of days the simulation has been running.
 * @author Christopher Helmer   
 * @version 1
 * @custom.inv Number of infected humans and birds, healthy humans and bird, and 
 * number of days the simulation has been running > = 0.
 *
 */
@SuppressWarnings("serial")
public class SimStatPanel extends JPanel {
    
    /**
     * Represents the number of rows for the layout.
     */
    private final int myRows = 10;
    /**
     * String that represents healthy.
     */
    private final String myHealthy = "Healthy: ";
    /**
     * String that represents infected.
     */
    private final String myInfected = "Infected: ";
    
    
    /**
     * Disabled JButton that displays the number of infected humans.
     */
    private JButton myInfectedHuman;
    /**
     * Disabled JButton that displays the number of healthy humans.
     */
    private JButton myHealthyHuman;
    /**
     * Disabled JButton that displays the number of infected birds.
     */
    private JButton myInfectedBird;
    /**
     * Disabled JButton that displays the number of healthy birds.
     */
    private JButton myHealthyBird;
    /**
     * Disabled JButton that displays the number of days the simulation 
     * has been running.
     */
    private JButton myDayButton;
    /**
     * Integer representation of the number of days the simulation has
     * been running.
     */
    private int myDays;
    
    /**
     * Constructor for the SimStatPanel class.
     * @custom.post A SimStatPanel is constructed.
     */
    public SimStatPanel() {
        myDays = 0;
        simStatSetup();
    }
    
    /**
     * Helper method that sets the layout of the panel, generates the buttons, 
     * and adds the buttons and labels to the panel.
     */
    private void simStatSetup() {
        setLayout(new GridLayout(myRows, 1));
        myInfectedHuman = new JButton(myInfected);
        myHealthyHuman = new JButton(myHealthy);
        myInfectedBird = new JButton(myInfected);
        myHealthyBird = new JButton(myHealthy);
        myDayButton = new JButton("" + myDays);
        myInfectedHuman.setEnabled(false);
        myHealthyHuman.setEnabled(false);
        myInfectedBird.setEnabled(false);
        myHealthyBird.setEnabled(false);
        myDayButton.setEnabled(false);
        add(new Label("Environment Counts: "));
        add(new Label("Human Counts: "));
        add(myInfectedHuman);
        add(myHealthyHuman);
        add(new Label("Bird Counts:"));
        add(myInfectedBird);
        add(myHealthyBird);
        add(new Label("Number of Days:"));
        add(myDayButton);
    }
    
    /**
     * Sets the text for the infected human button.
     * @param aHuman > = 0 is the number of infected humans.
     * @custom.post The number of infected humans is added to 
     * the text of the infected human button.
     */
    public void setInfectedHumanText(final int aHuman) {
        myInfectedHuman.setText(myInfected + aHuman);
    }
    
    /**
     * Sets the text for the healthy human button.
     * @param aHuman > = 0 is the number of healthy humans.
     * @custom.post The number of healthy humans is added to 
     * the text of the healthy human button.
     */
    public void setHealthyHumanText(final int aHuman) {
        myHealthyHuman.setText(myHealthy + aHuman);
    }
    
    /**
     * Sets the text for the infected birds button.
     * @param aBird > = 0 is the number of infected birds.
     * @custom.post The number of infected birds is added to 
     * the text of the infected birds button.
     */
    public void setInfectedBirdText(final int aBird) {
        myInfectedBird.setText(myInfected + aBird); 
    }
    
    /**
     * Sets the text for the healthy birds button.
     * @param aBird > = 0 is the number of healthy birds.
     * @custom.post The number of healthy birds is added to 
     * the text of the healthy birds button.
     */
    public void setHealthyBirdText(final int aBird) {
        myHealthyBird.setText(myHealthy + aBird);
    }
    
    /**
     * Sets the text for the days button.
     * @param aDay > = 0 is the number of days the simulation has been running.
     * @custom.post The number of days is added to the text of the days button.
     */
    public void setDayText(final int aDay) {
        myDayButton.setText("" + aDay);
    }
    
    /**
     * Gets the number of days the simulation has been running.
     * @return the number of days the simulation has been running.
     */
    public int getMyDays() {
        return myDays;
    }
    
    /**
     * Sets the number of days that the simulation has been running.
     * @param aDay > = 0 is the number of days that the simulation has been running.
     * @custom.post The number of days is set to the input number.
     */
    public void setMyDays(int aDay) {
        myDays = aDay;
    }

}
