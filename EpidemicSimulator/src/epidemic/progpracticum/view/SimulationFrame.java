/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;

/**
 * The frame containing all the panels for the epidemic simulation.
 * @author Christopher Helmer
 * @version 1
 *
 */
@SuppressWarnings("serial")
public class SimulationFrame extends JFrame {
    
    /**
     * The SimInputPanel used in the simulation.
     */
    private SimInputPanel myInputPanel;
    /**
     * The SimStatPanel used in the simulation.
     */
    private SimStatPanel myStatPanel;
    /**
     * The SimTransporsPanel used in the simulation.
     */
    private SimTransportPanel myTransportPanel;
    /**
     * The WorldPanel used in the simulation.
     */
    private WorldPanel myWorldPanel;
    /**
     * the SimWorld used in the simulation.
     */
    private SimWorld mySimWorld;
 
    /**
     * Constructor that adds all of the panels to the frame.
     */
    public SimulationFrame() {
        mySimWorld = new SimWorld();
        myWorldPanel = new WorldPanel();
        myStatPanel = new SimStatPanel();
        myInputPanel = new SimInputPanel(mySimWorld, myWorldPanel, myStatPanel);
        myTransportPanel = new SimTransportPanel(mySimWorld, myWorldPanel, 
                                                 myStatPanel, myInputPanel);
        add(myInputPanel, BorderLayout.PAGE_START);
        add(myStatPanel, BorderLayout.EAST);
        add(myTransportPanel, BorderLayout.SOUTH);
        add(myWorldPanel, BorderLayout.CENTER);
        pack();
    }

}
