/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.view;

import epidemic.progpracticum.model.Bird;
import epidemic.progpracticum.model.Human;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RectangularShape;
import java.util.HashMap;
import java.util.Map;


/**
 * The action listener that generates and paints the initial 
 * set of humans and birds onto the WorldPanel. 
 * @author Christopher Helmer
 * @version 1
 *
 */
public class GenerateListener implements ActionListener {
    
    /**
     * Represents the SimWorld used by this listener.
     */
    private SimWorld mySimWorld;
    /**
     * Represents the WorldPanel used by this listener.
     */
    private WorldPanel myWorld;
    /**
     * Represents the SimStatPanel used by this listener.
     */
    private SimStatPanel myStatPanel;
    /**
     * Represents the map of humans and points.
     */
    private Map<RectangularShape, Human> myHumanMap;
    /**
     * Represents the map of birds and points.
     */
    private Map<RectangularShape, Bird> myBirdMap;
    
    /**
     * Constructs the GenerateListener.
     * @param aSimWorld is the SimWorld used.
     * @param aWorld is the WorldPanel used.
     * @param aSimStatPanel is the SimStatPanel used.
     * @param aHumNum is the number of humans in the simulation.
     * @param aBirdNum is the number of birds in the simulation.
     * @param aPct is the percent of infected  birds in the simulation.
     * @custom.post The GenerateListener is constructed.
     */
    public GenerateListener(final SimWorld aSimWorld, final WorldPanel aWorld,
                            final SimStatPanel aSimStatPanel,
                            final int aHumNum, final int aBirdNum, final int aPct) {
        
        mySimWorld = aSimWorld;
        mySimWorld.addHuman(aHumNum);
        mySimWorld.addBird(aBirdNum, aPct);
        myWorld = aWorld;
        myStatPanel = aSimStatPanel;
        
    }
    /**
     * Paints all the birds and humans to the WorldPanel.
     * @param anEvent is the event.
     * @custom.post The humans and birds are painted to the WorldPanel.
     */
    @Override
    public void actionPerformed(ActionEvent anEvent) {
        mySimWorld.setMyInitialHumanMap(new HashMap<RectangularShape, Human>());
        mySimWorld.setMyInitialBirdMap(new HashMap<RectangularShape, Bird>());
        
        if (mySimWorld.getMyInitialHumanMap().isEmpty() 
                && mySimWorld.getMyInitialBirdMap().isEmpty()) {
            myHumanMap = mySimWorld.getMyHumanMap();
            myBirdMap = mySimWorld.getMyBirdMap();
        } else {
            myHumanMap = mySimWorld.getMyInitialHumanMap();
            myBirdMap = mySimWorld.getMyInitialBirdMap();
        }
        
        mySimWorld.setMyInitialHumanMap(myHumanMap);
        myWorld.setMyHumanMap(mySimWorld.getMyInitialHumanMap());
        
        mySimWorld.setMyInitialBirdMap(myBirdMap);
        myWorld.setMyBirdMap(mySimWorld.getMyInitialBirdMap());
        
        myStatPanel.setMyDays(0);
        myStatPanel.setInfectedHumanText(mySimWorld.getInfectedHumans());
        myStatPanel.setHealthyHumanText(mySimWorld.getHealthyHumans());
        myStatPanel.setInfectedBirdText(mySimWorld.getInfectedBirds()); 
        myStatPanel.setHealthyBirdText(mySimWorld.getHealthyBirds());
        myStatPanel.setDayText(myStatPanel.getMyDays());
        
        myWorld.repaint();
        
    }

}
