/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.model;

import java.awt.Point;

/**
 * A bird object that extends AbstractEntity.
 * @author Christopher Helmer   
 * @version 1
 * @custom.inv aDayNum >= 0, 0 <= x < world width, 0 <= y < world height.
 *
 */
public class Bird extends AbstractEntity {
    
    /**
     * Number of spaces to move the bird each time iteration.
     */
    private final int myMove = SimParams.BIRD_DIM / 2;
    
    /**
     * Constructor for the Bird class.
     * @param aSpecies is the species of the bird.
     * @param aSick is boolean whether the bird is sick.
     * @param aDayNum >= 0, is the number of days sick.
     * @param aLocation is the location of the bird in the world. 0 <= x < world width, 
     *                  0 <= y < world height
     */
    public Bird(final char aSpecies, final boolean aSick, 
                final int aDayNum, final Point aLocation) {
        super(aSpecies, aSick, aDayNum, aLocation);
        
    }
    
    /**
     * Move method uses a random number generator to determine movement in 
     * x and y direction. Moves in a horizontal or vertical direction between
     * 1 and 3 spaces(Each space is an increment of half the width of the bird diameter 
     * so that the bird appears to move one space).
     */
    @Override
    public void move() {
        final int move1 = SimParams.GENERATOR.nextInt(2);
        final int move2 = SimParams.GENERATOR.nextInt(6);
        int x_loc = this.getMyLocation().x;
        int y_loc = this.getMyLocation().y;
        if (move1 == 0) {
            if (move2 == 0) {
                x_loc -= myMove + myMove + myMove;
            } else if (move2 == 1) {
                x_loc -= myMove + myMove;
            } else if (move2 == 2) {
                x_loc -= myMove;
            } else if (move2 == myMove + 1) {
                x_loc += myMove;
            } else if (move2 == myMove + 2) {
                x_loc += myMove + myMove;
            } else {
                x_loc += myMove + myMove + myMove;
            }
        } else {
            if (move2 == 0) {
                y_loc -= myMove + myMove + myMove;
            } else if (move2 == 1) {
                y_loc -= myMove + myMove;
            } else if (move2 == 2) {
                y_loc -= myMove;
            } else if (move2 == myMove + 1) {
                y_loc += myMove;
            } else if (move2 == myMove + 2) {
                y_loc += myMove + myMove;
            } else {
                y_loc += myMove + myMove + myMove;
            } 
        }
        if (x_loc < 0) {
            x_loc = SimParams.PANEL_PIX_WIDTH - myMove;
        }
        if (x_loc > SimParams.PANEL_PIX_WIDTH - myMove) {
            x_loc = myMove;
        }
        if (y_loc < 0) {
            y_loc = SimParams.PANEL_PIX_HEIGHT - myMove;
        }
        if (y_loc > SimParams.PANEL_PIX_HEIGHT - myMove) {
            y_loc = myMove;
        } 
        setMyLocation(new Point(x_loc, y_loc));
        
    }

}
