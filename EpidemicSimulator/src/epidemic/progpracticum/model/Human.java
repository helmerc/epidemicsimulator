/**
 * Christopher Helmer. Final project Autumn 2013
 */
package epidemic.progpracticum.model;

import java.awt.Point;

/**
 * A human object that extends AbtractEntity.
 * @author Christopher Helmer
 * @version 1
 * @custom.inv aDayNum >= 0, 0 <= x < world width, 0 <= y < world height.
 *
 */
public class Human extends AbstractEntity {
    
    /**
     * Number of spaces to move the human each time iteration.
     */
    private final int myMove = SimParams.HUMAN_DIM / 2;

    /**
     * Constructor for the Human class.
     * @param aSpecies is the species of the human.
     * @param aSick is boolean whether the human is sick.
     * @param aDayNum >= 0, is the number of days sick.
     * @param aLocation is the location of the human in the world. 0 <= x < world width, 
     *                  0 <= y < world height
     */
    public Human(final char aSpecies, final boolean aSick, 
                 final int aDayNum, final Point aLocation) {
        super(aSpecies, aSick, aDayNum, aLocation);
        
    }
    
    /**
     * Move method uses a random number generator to determine movement in 
     * x and y direction(Each move is half the diameter of the human so that they
     * appear to move one space).
     */
    @Override
    public void move() {
        final int x_move = SimParams.GENERATOR.nextInt(3);
        final int y_move = SimParams.GENERATOR.nextInt(3);
        int x_loc = this.getMyLocation().x;
        int y_loc = this.getMyLocation().y;
        if (x_move == 0) {
            x_loc -= myMove; 
        } else if (x_move == 2) {
            x_loc += myMove;  
        }
        if (y_move == 0) {
            y_loc -= myMove;
        } else if (y_move == 2) {
            y_loc += myMove;
        }
        if (x_loc < 0) {
            x_loc = SimParams.PANEL_PIX_WIDTH - myMove;
        }
        if (x_loc > SimParams.PANEL_PIX_WIDTH - myMove) {
            x_loc = myMove;
        }
        if (y_loc < 0) {
            y_loc = SimParams.PANEL_PIX_HEIGHT - myMove;
        }
        if (y_loc > SimParams.PANEL_PIX_HEIGHT -myMove) {
            y_loc = myMove;
        }
        setMyLocation(new Point(x_loc, y_loc));
        
    }

}

